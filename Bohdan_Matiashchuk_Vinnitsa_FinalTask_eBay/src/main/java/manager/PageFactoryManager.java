package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }
    public CartPage getCartPage() {
        return new CartPage(driver);
    }
    public SellProductPage getSellProductPage() {
        return new SellProductPage(driver);
    }
	public PS5ProductPage getPS5ProductPage() {
		return new PS5ProductPage(driver);
	}
	public AdidasProductPage getAdidasProductPage() {
		return new AdidasProductPage(driver);
	}
    public RevlonPage getRevlonPage(){
        return new RevlonPage(driver);
    }
    public SignInPage getSignInPage (){ return new SignInPage(driver); }
    public SearchResultsPage getSearchResultsPage(){
        return new SearchResultsPage(driver);
    }
}