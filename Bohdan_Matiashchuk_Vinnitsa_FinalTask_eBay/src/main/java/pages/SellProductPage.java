package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SellProductPage extends BasePage{
    public SellProductPage(WebDriver driver) {
        super(driver);
    }
    private String brandName = "adidas shoes men";
    private static final long TIME_TO_WAIT = 100;

    @FindBy(xpath = "//a[@href = 'https://www.ebay.com/sl/prelist/suggest']")
    private WebElement listAnItemButton;
    @FindBy (xpath = "//input[@id='s0-0-0-30-9-keyword-box-input-textbox']")
    private WebElement brandInput;
    @FindBy (xpath = "//button[@aria-label='Edit category']")
    private WebElement editButton;
    @FindBy (xpath = "(//div[@class='se-field-card__content-value'])[2]")
    private WebElement chosenCategory;
    @FindBy (xpath = "//button[@class='textual-display btn btn--secondary prelist-radix__next-action']")
    private WebElement viewPossibleMatchesButton;
    @FindBy (xpath = "//button[@class='products-body__continue-btn btn btn--secondary']")
    private WebElement continueWithSelectionButton;
    @FindBy (xpath = "//input[@id='s0-0-0-30-13-3-6-condition-1000']")
    private WebElement radioButtonNew;
    @FindBy (xpath = "//button[@class='textual-display btn btn--primary prelist__next-action']")
    private WebElement nextButton;
    @FindBy (xpath = "//button[@class='keyword-suggestion__button btn btn--primary']")
    private WebElement noMoreSuggestion;

    public void clickOnListAnItemButton(){listAnItemButton.click(); }
    public void enterBrandName(){brandInput.sendKeys(brandName); }
    public void clickOnEditButton(){
        waitVisibilityOfElement(TIME_TO_WAIT, editButton);
        editButton.click(); }
    public void clickSearchButton(){
        waitVisibilityOfElement(TIME_TO_WAIT, noMoreSuggestion);
        noMoreSuggestion.click();
    }
    public void chooseCategory(){chosenCategory.click(); }
    public void clickOnViewPossibleMatches(){
        waitVisibilityOfElement(TIME_TO_WAIT, viewPossibleMatchesButton);
        viewPossibleMatchesButton.click(); }
    public void clickContinueWithSelectionButton(){
        waitVisibilityOfElement(TIME_TO_WAIT, continueWithSelectionButton);
        continueWithSelectionButton.click(); }
    public void clickOnRadioButtonNew(){ radioButtonNew.click(); }
    public void clickNextButton(){
        waitVisibilityOfElement(TIME_TO_WAIT, nextButton);
        nextButton.click(); }
}
