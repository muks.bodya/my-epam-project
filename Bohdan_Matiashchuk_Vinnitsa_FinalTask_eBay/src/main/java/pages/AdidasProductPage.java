package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertTrue;

public class AdidasProductPage extends BasePage{

    public AdidasProductPage(WebDriver driver) {
        super(driver);
    }

    private static final String ERROR_MESSAGE_TEXT = "Please select a Color";

    @FindBy(xpath = "//a[@id = 'binBtn_btn']")
    private WebElement buyItNowButton;
    @FindBy(xpath = "//div[@id='msku-sel-1-errMsg']")
    private WebElement errorMessage;
    @FindBy(xpath = "//a[@id='isCartBtn_btn']")
    private WebElement addToCartButton;
    @FindBy(xpath = "//div[@id='vi-atl-lnk']")
    private WebElement addToWishlistButton;
    @FindBy(xpath = "//select[@name = 'Colour']")
    private WebElement selectDropDown;
    @FindBy(xpath = "//select[@name = 'Colour']//option[1]")
    private WebElement colorButton;
    @FindBy(xpath = "//button[@id='sbin-gxo-btn']")
    private WebElement checkoutAsAGuestButton;

    public void clickOnBuyItNowButton() {
        buyItNowButton.click();
    }
    public void getErrorMessage() {
        assertTrue(ERROR_MESSAGE_TEXT,errorMessage.isDisplayed());
    }
    public void clickAddToCartButton() { addToCartButton.click(); }
    public void clickAddToWishlistButton() {
        addToWishlistButton.click();
    }
    public void clickOnSelect()  {
        selectDropDown.click();
    }
    public void chooseFromDropDownList() {
        colorButton.click();
    }
    public void checkoutAsAGuest() { checkoutAsAGuestButton.click(); }

    }

