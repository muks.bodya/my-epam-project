package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.junit.Assert.assertTrue;

public class HomePage extends BasePage{

    public HomePage(WebDriver driver) {
        super(driver);
    }

    private static final String URL_EBAY = "https://www.ebay.com/";
    private String product1 = "ADIDAS ORIGINALS ZX 750";
    private String product2 = "playstation 5";
    private static final  String ERROR_MESSAGE_WATCHLIST = "Please sign in to view items you are watching.";
    private static final String ERROR_MESSAGE_NOTIFICATION = "Please sign-in to view notifications.";
    private static final long TIME_TO_WAIT = 70;

    @FindBy(xpath = "//input [@class='gh-tb ui-autocomplete-input']")
    private WebElement searchBar;
    @FindBy(xpath = "//input [@value='Search']")
    private WebElement searchButton;
    @FindBy (xpath = "//a[@aria-label='Your shopping cart']")
    private WebElement cartButton;
    @FindBy (xpath = "//a[@href='https://www.ebay.com/mys/home?source=GBH']")
    private WebElement menuMyEbay;
    @FindBy (xpath = "//a[@class='gh-eb-li-a gh-rvi-menu watchlist-menu']")
    private WebElement watchlistIcon;
    @FindBy (xpath = "//span[contains(text(), 'to view')]")
    private WebElement errorMessageAboutWatchlist;
    @FindBy (xpath = "//button[@aria-controls='gh-eb-Alerts-o']")
    private WebElement notificationButton;
    @FindBy (xpath = "//a[@href='https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&ru=https%3A%2F%2Fwww.ebay.com%2F']")
    private WebElement errorMessageAboutNotification;
    @FindBy (xpath = "//li[@id='gh-shipto-click']")
    private WebElement shipToIcon;
    @FindBy (xpath = "//span[@class='expand-btn__cell menu-button__control--custom-label']")
    private WebElement selectCountryList;
    @FindBy (xpath = "//span [@class='menu-button']//div[@data-makeup-index='196']")
    private WebElement countryChoice;
    @FindBy (xpath = "//span[contains(text(), 'Done')]")
    private WebElement doneTextButton;
    @FindBy (xpath = "//button[@id='gh-shop-a']")
    private WebElement shopByCategoryButton;
    @FindBy (xpath = "//a[@href='https://www.ebay.com/b/Beauty/bn_7000259123']")
    private WebElement healthAndBeautyOption;
    @FindBy (xpath = "//a[@data-sp='m570.l1528']")
    private WebElement sellButton;

    public void openHomePage() {
        driver.get(URL_EBAY);
    }
	public void searchForProduct(){
        searchBar.sendKeys(product1);
}
    public void clickSearchButton(){
        searchButton.click();
    }
    public void clickOnTheCartButton() { cartButton.click(); }
    public void clickOnMenuMyEbay() { menuMyEbay.click(); }
    public void clickWatchlistIcon() { watchlistIcon.click(); }
    public void getErrorMessageAboutWatchlist() {
        waitVisibilityOfElement(TIME_TO_WAIT, errorMessageAboutWatchlist);
        assertTrue(ERROR_MESSAGE_WATCHLIST,errorMessageAboutWatchlist.isDisplayed()); }
    public void clickNotificationButton() { notificationButton.click(); }
    public void getErrorMessageAboutNotification() {
        waitVisibilityOfElement(TIME_TO_WAIT, errorMessageAboutNotification);
        errorMessageAboutNotification.isDisplayed();
        }
    public void clickShipTo() { shipToIcon.click(); }
    public void clickToSelectCountryFromList() { waitVisibilityOfElement(TIME_TO_WAIT, selectCountryList);
        selectCountryList.click();
    }
    public void selectCountryFromList() {
        waitVisibilityOfElement(TIME_TO_WAIT, countryChoice);
        countryChoice.click();
    }
    public void clickOnDone() { doneTextButton.click(); }
    public void searchForNewProduct(){
        searchBar.sendKeys(product2);
    }
	public void clickShopByCategoryButton(){
        shopByCategoryButton.click();
}
	public void clickOnHealthAndBeauty(){
        healthAndBeautyOption.click();
}
    public void clickOnSell(){sellButton.click(); }
}
