Feature: SmokeTest
As a user
I want to check main functionality of a website with 10 test cases
So that I check it works correctly

   # не проходить
  Scenario: User checks delivery options
    Given User opens homepage page
    And User searches by new keyword
    And User clicks search button
    And User opens new product
    And User clicks on shipping details
    And User selects change country
    And User selects country from dropdown
    And User presses Get rated
    And User changes Quantity
    Then User presses Get rated
  #проходить
  Scenario: Check Ship to
    Given User opens homepage page
    And User clicks on Ship to
    And User clicks on Select
    When User choose country
    Then User press Done
  #проходить
  Scenario: Check Watchlist/Notification error message on the Home page without sign in
    Given User opens homepage page
    And User clicks on the Watchlist
    And Message pops up about Watchlist
    When User clicks on the Bell
    Then Message pops up about notifications
#проходить
  Scenario: Check Cart/My Ebay error message on the Home page without sign in
    Given User opens homepage page
    And User clicks on the empty Cart
    And User gets message cart is empty
    When User clicks on My Ebay
    Then User refers to sign in page

    # не проходить
  Scenario: User creates to sell item without sign in
    Given User opens homepage page
    And User clicks on Sell
    And User clicks list an item
    And User enters brand name
    And User click Search
    When User click on view possible matches
    And User click on continue with selection
    And User selects condition
    And User press Next
    Then User refers to sign in page

    # не проходить
  Scenario: User places a bid at Action without sign in
    Given User opens homepage page
    And User clicks on Shop By Category
    And User chooses category
    And User chooses MakeUp Product
    And User chooses Lips
    And User chooses Lips stain
    When User chooses Action
    And User chooses action to participate
    And User places a bid
    Then User refers to sign in page

    # не проходить
  Scenario: Buy now/Add to Cart/ Add to Wishlist without choosing type
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    When User chooses product
    And User clicks on button Buy it now
    And User gets error message
    And User clicks on button Add to cart
    And User gets error message
    And User clicks on button Add to Wishlist
    Then User gets error message

     # не проходить
  Scenario: Buy it now product without sign in
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    And User chooses product
    And User click on select
    And User chooses from dropdown list
    When User clicks on button Buy it now
    Then User goes to checkout as guest

    # не проходить
  Scenario: Check Add to Cart without sign in
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    And User chooses product
    And User click on select
    And User chooses from dropdown list
    When User clicks on button Add to cart
    And User sees amount in a Cart
    And User click on Checkout button
    Then User refers to sign in page

    # не проходить
  Scenario: Add product to Wishlist without sign in
    Given User opens homepage page
    And User searches by keyword
    And User clicks search button
    And User chooses product
    And User click on select
    And User chooses from dropdown list
    When User clicks on button Add to Wishlist
    Then User refers to sign in page